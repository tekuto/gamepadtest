﻿#include "sucrose/util/export.h"
#include "gamepadtest/gamepadtest.h"

#include "sucrose/basesystem/data.h"
#include "sucrose/basesystem/args.h"
#include "sucrose/basesystem/context.h"
#include "sucrose/window/window.h"
#include "sucrose/window/painteventhandler.h"
#include "sucrose/window/paintevent.h"
#include "sucrose/window/eventhandlers.h"
#include "sucrose/gl/context.h"
#include "sucrose/gl/current.h"
#include "sucrose/gamepad/connecteventhandler.h"
#include "sucrose/gamepad/connectevent.h"
#include "sucrose/gamepad/disconnecteventhandler.h"
#include "sucrose/gamepad/disconnectevent.h"
#include "sucrose/gamepad/buttoneventhandler.h"
#include "sucrose/gamepad/buttonevent.h"
#include "sucrose/gamepad/axiseventhandler.h"
#include "sucrose/gamepad/axisevent.h"
#include "sucrose/gamepad/eventhandlers.h"
#include "sucrose/gamepad/id.h"
#include "sucrose/common/unique.h"

#include <memory>
#include <new>
#include <utility>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    struct GameData
    {
        enum Color
        {
            WHITE,
            BLUE,
            RED,
            GREEN,
            YELLOW,
        };

        Color   color;

        sucrose::Unique< sucrose::GLContext >   glContextUnique;

        sucrose::Unique< sucrose::WindowPaintEventHandler > paintEventHandlerUnique;

        sucrose::Unique< sucrose::GamepadConnectEventHandler >      connectEventHandlerUnique;
        sucrose::Unique< sucrose::GamepadDisconnectEventHandler >   disconnectEventHandlerUnique;
        sucrose::Unique< sucrose::GamepadButtonEventHandler >       buttonEventHandlerUnique;
        sucrose::Unique< sucrose::GamepadAxisEventHandler >         axisEventHandlerUnique;
    };

    void finalizeGameData(
        GameData &
    );

    struct FinalizeGameData
    {
        void operator()(
            GameData *  _gameData
        ) const
        {
            finalizeGameData( *_gameData );
        }
    };

    typedef std::unique_ptr<
        GameData
        , FinalizeGameData
    > GameDataUnique;

    void setColor(
        const GameData &    _GAME_DATA
    )
    {
        auto    r = float( 0 );
        auto    g = float( 0 );
        auto    b = float( 0 );
        switch( _GAME_DATA.color ) {
        case GameData::Color::WHITE:
            r = 1.0f;
            g = 1.0f;
            b = 1.0f;
            break;

        case GameData::Color::BLUE:
            r = 0.0f;
            g = 0.0f;
            b = 1.0f;
            break;

        case GameData::Color::RED:
            r = 1.0f;
            g = 0.0f;
            b = 0.0f;
            break;

        case GameData::Color::GREEN:
            r = 0.0f;
            g = 1.0f;
            b = 0.0f;
            break;

        case GameData::Color::YELLOW:
            r = 1.0f;
            g = 1.0f;
            b = 0.0f;
            break;

        default:
            r = 0.0f;
            g = 0.0f;
            b = 0.0f;
            break;
        }

        glColor3f(
            r
            , g
            , b
        );
    }

    void paintEvent(
        sucrose::WindowPaintEvent & _event
        , GameData &                _gameData
    )
    {
#ifdef  DEBUG
        std::printf( "I:x = %d\n", sucrose::getX( _event ) );
        std::printf( "I:y = %d\n", sucrose::getY( _event ) );
        std::printf( "I:w = %d\n", sucrose::getWidth( _event ) );
        std::printf( "I:h = %d\n", sucrose::getHeight( _event ) );
#endif  // DEBUG

        auto &  glContext = *( _gameData.glContextUnique );
        auto &  window = sucrose::getWindow( _event );

        auto    glCurrentUnique = sucrose::unique(
            sucrose::newGLCurrent(
                glContext
                , window
            )
        );
        if( glCurrentUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:OpenGLカレントの生成に失敗\n" );
#endif  // DEBUG

            return;
        }
        auto &  glCurrent = *glCurrentUnique;

        glClearColor( 0, 0, 0, 0 );
        glClear( GL_COLOR_BUFFER_BIT );

        setColor( _gameData );

        glBegin( GL_TRIANGLES );

        glVertex2f( 0, 0 );
        glVertex2f( 1, 0 );
        glVertex2f( 1, 1 );

        glEnd();

        sucrose::swapBuffers( glCurrent );
    }

    void changeColor(
        sucrose::BaseContext &  _context
        , GameData &            _gameData
        , GameData::Color       _color
    )
    {
        _gameData.color = _color;

        auto &  window = sucrose::getWindow( _context );

        sucrose::repaint(
            window
            , 0
            , 0
            , 640
            , 480
        );
    }

    void connectEvent(
        sucrose::GamepadConnectEvent &  _event
        , sucrose::BaseContext &        _context
        , GameData &                    _gameData
    )
    {
#ifdef  DEBUG
        std::printf(
            "ゲームパッド接続 ID:\"%s\", 名前:\"%s\", ボタン数:%u, 軸数:%u\n"
            , sucrose::getString( sucrose::getID( _event ) )
            , sucrose::getName( _event )
            , sucrose::getButtonCount( _event )
            , sucrose::getAxisCount( _event )
        );
#endif  // DEBUG

        changeColor(
            _context
            , _gameData
            , GameData::Color::BLUE
        );
    }

    void disconnectEvent(
        sucrose::GamepadDisconnectEvent &   _event
        , sucrose::BaseContext &            _context
        , GameData &                        _gameData
    )
    {
#ifdef  DEBUG
        std::printf(
            "ゲームパッド切断 ID:\"%s\"\n"
            , sucrose::getString( sucrose::getID( _event ) )
        );
#endif  // DEBUG

        changeColor(
            _context
            , _gameData
            , GameData::Color::RED
        );
    }

    void buttonEvent(
        sucrose::GamepadButtonEvent &   _event
        , sucrose::BaseContext &        _context
        , GameData &                    _gameData
    )
    {
#ifdef  DEBUG
        std::printf(
            "ボタンイベント ID:\"%s\", 番号:%u, 状態:%s\n"
            , sucrose::getString( sucrose::getID( _event ) )
            , sucrose::getIndex( _event )
            , sucrose::getPressed( _event ) ? "ON" : "OFF"
        );
#endif  // DEBUG

        changeColor(
            _context
            , _gameData
            , GameData::Color::GREEN
        );
    }

    void axisEvent(
        sucrose::GamepadAxisEvent & _event
        , sucrose::BaseContext &    _context
        , GameData &                _gameData
    )
    {
#ifdef  DEBUG
        std::printf(
            "軸イベント ID:\"%s\", 番号:%u, 状態:%d\n"
            , sucrose::getString( sucrose::getID( _event ) )
            , sucrose::getIndex( _event )
            , sucrose::getValue( _event )
        );
#endif  // DEBUG

        changeColor(
            _context
            , _gameData
            , GameData::Color::YELLOW
        );
    }

    GameData * initializeGameData(
        sucrose::BaseContext &  _context
    )
    {
        auto    glContextUnique = sucrose::unique( sucrose::newGLContext() );
        if( glContextUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:OpenGLコンテキスト生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    gameDataUnique = std::unique_ptr< GameData >(
            new( std::nothrow )GameData{
                GameData::Color::WHITE,
                std::move( glContextUnique ),
            }
        );
        if( gameDataUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:ゲームデータの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        auto &  gameData = *gameDataUnique;

        auto &  paintEventHandlerUnique = gameData.paintEventHandlerUnique;
        paintEventHandlerUnique.reset(
            sucrose::newWindowPaintEventHandler(
                [
                    &gameData
                ]
                (
                    sucrose::WindowPaintEvent & _event
                )
                {
                    paintEvent(
                        _event
                        , gameData
                    );
                }
            )
        );
        if( paintEventHandlerUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto &  connectEventHandlerUnique = gameData.connectEventHandlerUnique;
        connectEventHandlerUnique.reset(
            sucrose::newGamepadConnectEventHandler(
                [
                    &_context
                    , &gameData
                ]
                (
                    sucrose::GamepadConnectEvent &  _event
                )
                {
                    connectEvent(
                        _event
                        , _context
                        , gameData
                    );
                }
            )
        );
        if( connectEventHandlerUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto &  disconnectEventHandlerUnique = gameData.disconnectEventHandlerUnique;
        disconnectEventHandlerUnique.reset(
            sucrose::newGamepadDisconnectEventHandler(
                [
                    &_context
                    , &gameData
                ]
                (
                    sucrose::GamepadDisconnectEvent &   _event
                )
                {
                    disconnectEvent(
                        _event
                        , _context
                        , gameData
                    );
                }
            )
        );
        if( disconnectEventHandlerUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto &  buttonEventHandlerUnique = gameData.buttonEventHandlerUnique;
        buttonEventHandlerUnique.reset(
            sucrose::newGamepadButtonEventHandler(
                [
                    &_context
                    , &gameData
                ]
                (
                    sucrose::GamepadButtonEvent &   _event
                )
                {
                    buttonEvent(
                        _event
                        , _context
                        , gameData
                    );
                }
            )
        );
        if( buttonEventHandlerUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto &  axisEventHandlerUnique = gameData.axisEventHandlerUnique;
        axisEventHandlerUnique.reset(
            sucrose::newGamepadAxisEventHandler(
                [
                    &gameData
                    , &_context
                ]
                (
                    sucrose::GamepadAxisEvent & _event
                )
                {
                    axisEvent(
                        _event
                        , _context
                        , gameData
                    );
                }
            )
        );
        if( axisEventHandlerUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return gameDataUnique.release();
    }

    void finalizeGameData(
        GameData &  _gameData
    )
    {
        delete &_gameData;
    }

    bool initWindowEventHandlers(
        sucrose::BasesystemArgs &   _args
        , GameData &                _gameData
    )
    {
        auto    eventHandlersUnique = sucrose::unique( sucrose::newWindowEventHandlers() );
        if( eventHandlersUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラ集合の生成に失敗\n" );
#endif  // DEBUG

            return false;
        }
        auto &  eventHandlers = *eventHandlersUnique;

        if( sucrose::add(
            eventHandlers
            , *( _gameData.paintEventHandlerUnique )
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラ集合へのイベントハンドラ追加に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( sucrose::setWindowEventHandlers(
            _args
            , eventHandlers
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラ集合の設定に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initGamepadManagerEventHandlers(
        sucrose::BasesystemArgs &   _args
        , GameData &                _gameData
    )
    {
        auto    eventHandlersUnique = sucrose::unique( sucrose::newGamepadManagerEventHandlers() );
        if( eventHandlersUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラ集合の生成に失敗\n" );
#endif  // DEBUG

            return false;
        }
        auto &  eventHandlers = *eventHandlersUnique;

        if( sucrose::add(
            eventHandlers
            , *( _gameData.connectEventHandlerUnique )
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラ集合へのイベントハンドラ追加に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( sucrose::add(
            eventHandlers
            , *( _gameData.disconnectEventHandlerUnique )
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラ集合へのイベントハンドラ追加に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( sucrose::add(
            eventHandlers
            , *( _gameData.buttonEventHandlerUnique )
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラ集合へのイベントハンドラ追加に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( sucrose::add(
            eventHandlers
            , *( _gameData.axisEventHandlerUnique )
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラ集合へのイベントハンドラ追加に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( sucrose::setGamepadManagerEventHandlers(
            _args
            , eventHandlers
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラ集合の設定に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }
}

namespace gamepadtest {
    bool initializeGamepadTest(
        sucrose::BasesystemData &   _basesystemData
    )
    {
        auto &  args = sucrose::getBasesystemArgs( _basesystemData );
        auto &  context = sucrose::getBaseContext( _basesystemData );

        auto    gameDataUnique = GameDataUnique(
            initializeGameData(
                context
            )
        );
        if( gameDataUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:ゲームデータの生成に失敗\n" );
#endif  // DEBUG

            return false;
        }
        auto &  gameData = *gameDataUnique;

        if( sucrose::setGameData(
            args
            , gameData
            , finalizeGameData
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ゲームデータの設定に失敗\n" );
#endif  // DEBUG

            return false;
        }
        gameDataUnique.release();

        if( initWindowEventHandlers(
            args
            , gameData
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ウィンドウイベントハンドラの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initGamepadManagerEventHandlers(
            args
            , gameData
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ゲームパッドマネージャイベントハンドラの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }
}
