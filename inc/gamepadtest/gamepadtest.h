﻿#ifndef GAMEPADTEST_GAMEPADTEST_H
#define GAMEPADTEST_GAMEPADTEST_H

#include "sucrose/basesystem/data.h"
#include "sucrose/util/import.h"

namespace gamepadtest {
    SUCROSE_FUNCTION_BOOL(
        initializeGamepadTest(
            sucrose::BasesystemData &
        )
    )
}

#endif  // GAMEPADTEST_GAMEPADTEST_H
