# -*- coding: utf-8 -*-

_MAJOR_VERSION = 0
_MINOR_VERSION = 2
_REVISION = 0

def getString(
):
    return '.'.join(
        str( i )
        for i in [
            _MAJOR_VERSION,
            _MINOR_VERSION,
            _REVISION,
        ]
    )
